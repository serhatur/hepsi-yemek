﻿namespace HepsiYemek.Data.Dtos
{
    using HepsiYemek.Data.Models;
    using System.ComponentModel.DataAnnotations;
    public class ProductDto
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "Name field is required")]
        public string Name { get; set; }

        public string Description { get; set; }
        public CategoryDto Category { get; set; }

        [Required(ErrorMessage = "Price field is required")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Currency field is required")]
        public string Currency { get; set; }
    }
}
