﻿namespace HepsiYemek.Services
{
    using HepsiYemek.Data.Dtos;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    public interface ICategoryService
    {
        Task<Response<List<CategoryDto>>> GetAllAsync();
        Task<Response<CategoryDto>> CreateAsync(CategoryDto categoryDto);
        Task<Response<CategoryDto>> GetByIdAsync(string id);
        Task<Response<CategoryDto>> UpdateAsync(CategoryDto categoryDto);
        Task<Response<CategoryDto>> DeleteAsync(string id);
    }
}
