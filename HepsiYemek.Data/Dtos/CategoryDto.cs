﻿using System.ComponentModel.DataAnnotations;

namespace HepsiYemek.Data.Dtos
{
    public class CategoryDto
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "Name field is required")]
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
