﻿namespace HepsiYemek.API.Controllers
{
    using HepsiYemek.Data.Dtos;
    using HepsiYemek.Services;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Threading.Tasks;

    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        [Route("/api/[controller]/{id}")]
        public async Task<IActionResult> GetById(string id)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var product = await _productService.GetByIdAsync(id);
            return Ok(product);
        }

        [HttpGet]
        [Route("/api/[controller]")]
        public async Task<IActionResult> GetAll()
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var products = await _productService.GetAllAsync();
            return Ok(products);
        }


        [HttpPost]
        public async Task<IActionResult> Create(ProductDto productDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var response = await _productService.CreateAsync(productDto);
            return Ok(response);
        }

        [HttpPut]
        public async Task<IActionResult> Update(ProductDto productUpdateDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var response = await _productService.UpdateAsync(productUpdateDto);
            return Ok(response);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var response = await _productService.DeleteAsync(id);
            return Ok(response);
        }
    }
}
