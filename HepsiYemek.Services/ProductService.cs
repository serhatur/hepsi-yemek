﻿namespace HepsiYemek.Services
{
    using AutoMapper;
    using HepsiYemek.Core.Caching;
    using HepsiYemek.Data;
    using HepsiYemek.Data.Dtos;
    using HepsiYemek.Data.Models;
    using MongoDB.Driver;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class ProductService : IProductService
    {
        private readonly IMongoContext _context;
        private readonly IMapper _mapper;
        private readonly IRedisCacheService _redisCacheService;
        public ProductService(IMongoContext context, IMapper mapper, IRedisCacheService redisCacheService)
        {
            _context = context;
            _mapper = mapper;
            _redisCacheService = redisCacheService;
        }

        public async Task<Response<List<ProductDto>>> GetAllAsync()
        {
            var categories = await _context.Products.Find(category => true).ToListAsync();
            return Response<List<ProductDto>>.Success(_mapper.Map<List<ProductDto>>(categories), 200);
        }

        public async Task<Response<ProductDto>> CreateAsync(ProductDto productDto)
        {
            var product = _mapper.Map<Product>(productDto);
            await _context.Products.InsertOneAsync(product);

            return Response<ProductDto>.Success(_mapper.Map<ProductDto>(product), 200);
        }

        public async Task<Response<ProductDto>> GetByIdAsync(string id)
        {
            if (_redisCacheService.Any(string.Format("Product", id)))
            {
                var cachedProduct = _redisCacheService.Get<Product>(string.Format("Product", id));
                return Response<ProductDto>.Success(_mapper.Map<ProductDto>(cachedProduct), 200);
            }

            var product = await _context.Products.Find<Product>(x => x.Id == id).FirstOrDefaultAsync();
            var category = await _context.Categories.Find<Category>(x => x.Id == product.Category.Id).FirstOrDefaultAsync();

            if (product == null)
            {
                return Response<ProductDto>.Fail("Category not found", 404);
            }

            _redisCacheService.Add(string.Format("Product", id), product);
            _redisCacheService.SetTTL("Product", TimeSpan.FromMinutes(5));

            var productResponse = _mapper.Map<ProductDto>(product);
            productResponse.Category = _mapper.Map<CategoryDto>(category);

            return Response<ProductDto>.Success(_mapper.Map<ProductDto>(productResponse), 200); ;
        }

        public async Task<Response<ProductDto>> UpdateAsync(ProductDto productUpdateDto)
        {
            var product = await GetByIdAsync(productUpdateDto.Id);
            if (product == null)
                return Response<ProductDto>.Fail("Product not found", 404);

            var category = await _context.Categories.Find<Category>(x => x.Id == product.Data.Category.Id).FirstOrDefaultAsync();

            var updateProduct = _mapper.Map<Product>(productUpdateDto);
            updateProduct.Category.Id = category.Id;

            var result = await _context.Products.FindOneAndReplaceAsync(x => x.Id == productUpdateDto.Id, updateProduct);

            return Response<ProductDto>.Success(204);
        }

        public async Task<Response<ProductDto>> DeleteAsync(string id)
        {
            var result = await _context.Products.DeleteOneAsync(x => x.Id == id);

            if (result.DeletedCount > 0)
                return Response<ProductDto>.Success(204);
            else
                return Response<ProductDto>.Fail("Product not found", 404);
        }
    }
}
