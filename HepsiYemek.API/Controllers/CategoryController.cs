﻿namespace HepsiYemek.API.Controllers
{
    using HepsiYemek.Data.Dtos;
    using HepsiYemek.Services;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [HttpGet]
        [Route("/api/[controller]/{id}")]
        public async Task<IActionResult> GetById(string id)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var category = await _categoryService.GetByIdAsync(id);
            return Ok(category);
        }

        [HttpGet]
        [Route("/api/[controller]")]
        public async Task<IActionResult> GetAll()
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var categories = await _categoryService.GetAllAsync();
            return Ok(categories);
        }


        [HttpPost]
        public async Task<IActionResult> Create(CategoryDto categoryDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var response = await _categoryService.CreateAsync(categoryDto);
            return Ok(response);
        }

        [HttpPut]
        public async Task<IActionResult> Update(CategoryDto courseUpdateDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var response = await _categoryService.UpdateAsync(courseUpdateDto);
            return Ok(response);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var response = await _categoryService.DeleteAsync(id);
            return Ok(response);
        }
    }
}
