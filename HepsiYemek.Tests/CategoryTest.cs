using HepsiYemek.API.Controllers;
using HepsiYemek.Data.Dtos;
using HepsiYemek.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace HepsiYemek.Tests
{
    public class CategoryTest
    {
        private readonly Mock<ICategoryService> mockCategoryService;
        private readonly CategoryController categoryController;
        public CategoryTest()
        {
            mockCategoryService = new Mock<ICategoryService>();
            categoryController = new CategoryController(mockCategoryService.Object);
        }

        [Fact]
        public async Task GetByIdAsync_ShouldReturn_ReturnsCategory_WithValidId()
        {
            var mockCategoryResponse = new Response<CategoryDto>();

            mockCategoryService.Setup(c => c.GetByIdAsync(It.IsAny<string>())).ReturnsAsync(mockCategoryResponse);

            var okResult = await categoryController.GetById("613b4e128c3790d2bf8dd8e0");
            ObjectResult okObjectResult = (ObjectResult)okResult;
            Assert.Equal(mockCategoryResponse, okObjectResult.Value);
            mockCategoryService.VerifyAll();
        }

        [Fact]
        public async Task GetAllAsync_ShouldReturn_Categories()
        {
            var mockCategoryResponse = new Response<List<CategoryDto>>();

            mockCategoryService.Setup(c => c.GetAllAsync()).ReturnsAsync(mockCategoryResponse);

            var okResult = await categoryController.GetAll();
            ObjectResult okObjectResult = (ObjectResult)okResult;
            Assert.Equal(mockCategoryResponse, okObjectResult.Value);
            mockCategoryService.VerifyAll();
        }
    }
}
