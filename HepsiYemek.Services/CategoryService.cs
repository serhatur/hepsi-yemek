﻿

namespace HepsiYemek.Services
{
    using AutoMapper;
    using HepsiYemek.Data;
    using HepsiYemek.Data.Dtos;
    using HepsiYemek.Data.Models;
    using MongoDB.Driver;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    public class CategoryService : ICategoryService
    {
        private readonly IMongoContext _context;
        private readonly IMapper _mapper;

        public CategoryService(IMongoContext context,IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<Response<List<CategoryDto>>> GetAllAsync()
        {
            var categories = await _context.Categories.Find(category => true).ToListAsync();
            return Response<List<CategoryDto>>.Success(_mapper.Map<List<CategoryDto>>(categories), 200);
        }

        public async Task<Response<CategoryDto>> CreateAsync(CategoryDto categoryDto)
        {
            var category = _mapper.Map<Category>(categoryDto);
            await _context.Categories.InsertOneAsync(category);

            return Response<CategoryDto>.Success(_mapper.Map<CategoryDto>(category), 200);
        }

        public async Task<Response<CategoryDto>> GetByIdAsync(string id)
        {
            var category = await _context.Categories.Find<Category>(x => x.Id == id).FirstOrDefaultAsync();

            if (category == null)
                return Response<CategoryDto>.Fail("Category not found", 404);

            return Response<CategoryDto>.Success(_mapper.Map<CategoryDto>(category), 200);
        }

        public async Task<Response<CategoryDto>> UpdateAsync(CategoryDto courseUpdateDto)
        {
            var updateCourse = _mapper.Map<Category>(courseUpdateDto);
            var result = await _context.Categories.FindOneAndReplaceAsync(x => x.Id == courseUpdateDto.Id, updateCourse);

            if (result == null)
                return Response<CategoryDto>.Fail("Category not found", 404);

            return Response<CategoryDto>.Success(204);
        }

        public async Task<Response<CategoryDto>> DeleteAsync(string id)
        {
            var result = await _context.Categories.DeleteOneAsync(x => x.Id == id);

            if (result.DeletedCount > 0)
                return Response<CategoryDto>.Success(204);
            else
                return Response<CategoryDto>.Fail("Category not found", 404);
        }
    }
}
