﻿namespace HepsiYemek.Data
{
    using HepsiYemek.Data.Models;
    using HepsiYemek.Data.Settings;
    using MongoDB.Driver;
    public class MongoContext : IMongoContext
    {
        public MongoContext(DatabaseSettings databaseSettings)
        {
            if (Client == null)
            {
                try
                {
                    Client = new MongoClient(databaseSettings.ConnectionString);

                    Database = Client.GetDatabase(databaseSettings.DatabaseName);
                }
                catch
                {
                    throw;
                }
            }

            Products = Database.GetCollection<Product>(databaseSettings.ProductsCollectionName);

            Categories = Database.GetCollection<Category>(databaseSettings.CategoriesCollectionName);
        }

        public MongoClient Client { get; }
        public IMongoDatabase Database { get; }
        public IMongoCollection<Product> Products { get; set; }
        public IMongoCollection<Category> Categories { get; set; }
    }

    public interface IMongoContext
    {
        MongoClient Client { get; }
        IMongoDatabase Database { get; }
        IMongoCollection<Product> Products { get; set; }
        IMongoCollection<Category> Categories { get; set; }
    }
}
