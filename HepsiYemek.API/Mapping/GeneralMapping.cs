﻿namespace HepsiYemek.API.Mapping
{
    using AutoMapper;
    using HepsiYemek.Data.Dtos;
    using HepsiYemek.Data.Models;

    public class GeneralMapping : Profile
    {
        public GeneralMapping()
        {
            CreateMap<Product, ProductDto>().ReverseMap();
            CreateMap<Category, CategoryDto>().ReverseMap();
        }
    }
}

