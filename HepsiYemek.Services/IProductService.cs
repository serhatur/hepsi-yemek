﻿namespace HepsiYemek.Services
{
    using HepsiYemek.Data.Dtos;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    public interface IProductService
    {
        Task<Response<List<ProductDto>>> GetAllAsync();
        Task<Response<ProductDto>> CreateAsync(ProductDto productDto);
        Task<Response<ProductDto>> GetByIdAsync(string id);
        Task<Response<ProductDto>> UpdateAsync(ProductDto productDto);
        Task<Response<ProductDto>> DeleteAsync(string id);
    }
}
