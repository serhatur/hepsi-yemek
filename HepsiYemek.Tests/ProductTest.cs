﻿namespace HepsiYemek.Tests
{
    using HepsiYemek.API.Controllers;
    using HepsiYemek.Data.Dtos;
    using HepsiYemek.Services;
    using Microsoft.AspNetCore.Mvc;
    using Moq;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Xunit;
    public class ProductTest
    {
        private readonly Mock<IProductService> mockProductService;
        private readonly ProductController productController;

        public ProductTest()
        {
            mockProductService = new Mock<IProductService>();
            productController = new ProductController(mockProductService.Object);
        }

        [Fact]
        public async Task GetByIdAsync_ShouldReturn_ReturnsProduct_WithValidId()
        {
            var mockProductResponse = new Response<ProductDto>();
            mockProductService.Setup(c => c.GetByIdAsync(It.IsAny<string>())).ReturnsAsync(mockProductResponse);

            var okResult = await productController.GetById("613b59134e5874263ac17d70");
            ObjectResult okObjectResult = (ObjectResult)okResult;
            Assert.Equal(mockProductResponse, okObjectResult.Value);
            mockProductService.VerifyAll();
        }

        [Fact]
        public async Task GetAllAsync_ShouldReturn_Products()
        {
            var mockProductResponse = new Response<List<ProductDto>>();

            mockProductService.Setup(c => c.GetAllAsync()).ReturnsAsync(mockProductResponse);

            var okResult = await productController.GetAll();
            ObjectResult okObjectResult = (ObjectResult)okResult;
            Assert.Equal(mockProductResponse, okObjectResult.Value);
            mockProductService.VerifyAll();
        }
    }
}
